//
//  mbv_image_arithmetic.hpp
//

#ifndef mbv_image_arithmetic_hpp
#define mbv_image_arithmetic_hpp

namespace mbv
{

    //! Add a scalar value to each pixel of f_image.
    //! NOTE: result is not checked whether it is in the pixel range (overflow possible).
    template<class ImageType>
    void addScalarToImage(const typename ImageType::pixel_type& f_add, ImageType& f_image)
    {
        //TODO
    }

}// namespace

#endif
