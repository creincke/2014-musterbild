//
//  mbv_image.hpp
//

#ifndef mbv_image_hpp
#define mbv_image_hpp

#include <memory>

//! namespace for "mustererkennung und bildverarbeitung"
namespace mbv {

    template <class PixelType, class AllocatorType = std::allocator<PixelType> >
    class TImage {
    public:
        //! Type of allocator;
        typedef AllocatorType allocator_type;
        
        //! Pixel type of image.
        typedef PixelType pixel_type;
        
        //! C'tor, allocates an image of zero (0 x 0) size
        TImage(const allocator_type& f_allocator = allocator_type());
        //! C'tor, allocates an image of f_width*f_height
        explicit TImage(int f_width, int f_height, const allocator_type& f_allocator = allocator_type());
        //! D'tor
        ~TImage();

        //! resize image
        void resize(int f_width, int f_height);
                
        //! Return height of image.
        int getHeight() const;
        //! Return width of image.
        int getWidth() const;

        //! Return a const reference to the pixel at position f_x and f_y.
        //! NOTE: position is not checked if in image.
        const PixelType& operator()(int f_x, int f_y) const;
        //! Return a reference to the pixel at position f_x and f_y.
        //! NOTE: position is not checked if in image.
        PixelType& operator()(int f_x, int f_y);

    private:
        //! Allocator.
        allocator_type m_allocator;

        //! Storage of width and height of image.
        int m_width, m_height;
        //! Pointer to the pixel data.
        PixelType* m_data;
};
    
}//end namespace mbv

#include "mbv_image.inl"

#endif
