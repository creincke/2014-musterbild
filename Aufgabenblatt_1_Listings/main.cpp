//
//  main.cpp
//

#include "mbv_image.hpp"
#include "mbv_image_arithmetic.hpp"
#include "mbv_image_io.hpp"

#include <string>
#include <iostream>

using namespace mbv;

int main(int argc, const char * argv[])
{
    typedef TImage<unsigned char> image_t;
    image_t pgmImage;
    
    std::string filename("feep.pgm");

    readPGM(filename, pgmImage);
    
    addScalarToImage(5, pgmImage);
    
    savePGM(filename, pgmImage);
    
    return 0;
}

