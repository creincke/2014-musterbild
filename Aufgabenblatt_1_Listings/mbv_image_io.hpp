//
//  mbv_image_io.h
//

#ifndef mbv_image_io_hpp
#define mbv_image_io_hpp

#include "mbv_image.hpp"

#include <string>

namespace mbv
{
    template <class ImageType>
    bool readPGM(const std::string& f_filename, ImageType& f_image);

    template <class ImageType>
    bool savePGM(const std::string& f_filename, const ImageType& f_image);

    
} // namespace mbv

#include "mbv_image_io.inl"

#endif
